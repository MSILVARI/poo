package testfecha;

public class Fecha {

    //Atributos en private
    private int dia;
    private int mes;
    private int anio;
    private String texto1;
    private String texto2;
    private String texto3;

    //constructor por defecto o vacio
    public Fecha() {

    }
    // constructor con parametros    

    public Fecha(int dia, int mes, int anio) {
        this.dia = dia;
        this.mes = mes;
        this.anio = anio;
    }

   
    public Fecha(String texto1, String texto2, String texto3) {
        this.texto1 = texto1;
        this.texto2 = texto2;
        this.texto3 = texto3;
    }

   
    //setters y getters
    public int getDia() {
        return dia;
    }

    public void setDia(int d) {
        this.dia = d;
    }

    public int getMes() {
        return mes;
    }

    public void setMes(int m) {
        this.mes = m;
    }

    public int getAnio() {
        return anio;
    }

    public void setAnio(int a) {
        this.anio = a;
    }

    public String getTexto1() {
        return texto1;
    }

    public void setTexto1(String texto1) {
        this.texto1 = texto1;
    }

    public String getTexto2() {
        return texto2;
    }

    public void setTexto2(String texto2) {
        this.texto2 = texto2;
    }

    public String getTexto3() {
        return texto3;
    }

    public void setTexto3(String texto3) {
        this.texto3 = texto3;
    }

//Método toString para mostrar la fecha
    @Override
    public String toString() {

        StringBuilder sb = new StringBuilder();
        if (dia < 10) {
            sb.append("0");
        }
        sb.append(dia);
        sb.append("-");
        if (mes < 10) {
            sb.append("0");
        }
        sb.append(mes);
        sb.append("-");
        sb.append(anio);
        return sb.toString();
    }

    public boolean aquals(Object o) {
        Fecha otra = (Fecha) o;
        return (dia == otra.dia && (mes == otra.mes && (anio == otra.anio)));
    }

}
